import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:iot_final/services/google_sign_in.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
        body: SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(18),
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Image(image: AssetImage('assets/download.png')),
              const Padding(
                padding: EdgeInsets.only(top: 60.0),
                child: Text('Por favor, inicie sesión',
                    style: TextStyle(fontWeight: FontWeight.w500)),
              ),
              GestureDetector(
                onTap: () async {
                  final loginService = LoginService();
                  await loginService.loginGoogle();
                  // ignore: use_build_context_synchronously
                  Navigator.pushNamed(context, 'home');
                },
                child: Container(
                  padding: const EdgeInsets.all(3),
                  margin: EdgeInsets.symmetric(
                      horizontal: size.width * 0.2, vertical: size.width * 0.1),
                  width: double.infinity,
                  height: 45,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(7),
                      boxShadow: const [
                        BoxShadow(
                            offset: Offset(2, 1),
                            blurRadius: 6,
                            color: Color.fromARGB(255, 232, 232, 232))
                      ]),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: const [
                        FaIcon(
                          FontAwesomeIcons.google,
                          color: Color(0xff515151),
                          size: 22,
                        ),
                        Text(
                          "Ingrese con Google",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      ]),
                ),
              ),
              SizedBox(
                height: size.height * .1,
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
