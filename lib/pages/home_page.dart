import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:iot_final/models/response_data.dart';
import 'package:iot_final/models/sensor_variables.dart';
import 'package:iot_final/services/data_service.dart';
import 'package:iot_final/widgets/app_bar_custom.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int? defaultChoiceIndex;
  final List<String> _choicesList = [
    'Sensor 1',
    'Sensor 2',
    'Sensor 3',
    'Sensor 4',
    'Sensor 5',
    'Sensor 6',
    'Sensor 7',
    'Sensor 8',
    'Sensor 9',
    'Sensor 10',
  ];

  @override
  void initState() {
    super.initState();
    defaultChoiceIndex = 0;
  }

  @override
  Widget build(BuildContext context) {
    User user = FirebaseAuth.instance.currentUser!;
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 231, 231, 231),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AppBarCustom(name: user.displayName!, profileImage: user.photoURL!),
          SizedBox(
            height: size.height * 0.05,
          ),
          FutureBuilder(
            future: DataService.getVariableData(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const CupertinoActivityIndicator();
              } else {
                return InfoList(data: snapshot.data);
              }
            },
          ),
          SizedBox(
            height: size.height * 0.05,
          ),
          Container(
            margin: const EdgeInsets.only(left: 30),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              const Text(
                "Dispositivos",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: size.height * 0.02,
              ),
              _dispositivosList(),
              SizedBox(
                height: size.height * 0.03,
              ),
              Container(
                margin: const EdgeInsets.only(right: 30),
                height: 90,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                ),
                child: _SensorInfo(),
              )
            ]),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            padding:
                const EdgeInsets.only(left: 30, right: 30, top: 40, bottom: 5),
            height: size.height * 0.35,
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(45), topRight: Radius.circular(45)),
            ),
            child: Column(children: [
              FutureBuilder(
                future: DataService.getData(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const CupertinoActivityIndicator();
                  } else {
                    return HistorialRow(data: snapshot.data);
                  }
                },
              ),
            ]),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {});
        },
        backgroundColor: const Color(0xff333333),
        child: const Icon(Icons.refresh),
      ),
    );
  }

  SizedBox _dispositivosList() {
    return SizedBox(
      height: 35,
      child: Container(
        margin: const EdgeInsets.only(left: 30),
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          itemCount: _choicesList.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: ChoiceChip(
                label: Text(
                  _choicesList[index],
                  style: Theme.of(context)
                      .textTheme
                      .bodyText2!
                      .copyWith(color: Colors.white, fontSize: 14),
                ),
                selected: defaultChoiceIndex == index,
                backgroundColor: const Color(0xffbcbcbc),
                selectedColor: const Color(0xff2E55E9),
                onSelected: (value) {
                  setState(() {
                    defaultChoiceIndex = value ? index : defaultChoiceIndex;
                  });
                },
              ),
            );
          },
        ),
      ),
    );
  }
}

class InfoList extends StatelessWidget {
  final SensorVariables data;
  const InfoList({
    required this.data,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30),
      height: 90,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
      ),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
        Row(
          children: [
            Container(
              height: 40,
              width: 40,
              decoration: BoxDecoration(
                  color: const Color(0xff2E55E9),
                  borderRadius: BorderRadius.circular(20)),
              child: const Center(
                  child: FaIcon(
                FontAwesomeIcons.satelliteDish,
                color: Colors.white,
                size: 16,
              )),
            ),
            const SizedBox(
              width: 10,
            ),
            Text(data.count.toString(),
                style: const TextStyle(fontWeight: FontWeight.bold))
          ],
        ),
        Row(
          children: [
            Container(
              height: 40,
              width: 40,
              decoration: BoxDecoration(
                  color: const Color(0xffff661e),
                  borderRadius: BorderRadius.circular(20)),
              child: const Center(
                  child: FaIcon(
                FontAwesomeIcons.temperatureHigh,
                color: Colors.white,
                size: 16,
              )),
            ),
            const SizedBox(
              width: 10,
            ),
            const Text("32°", style: TextStyle(fontWeight: FontWeight.bold))
          ],
        ),
        Row(
          children: [
            Container(
              height: 40,
              width: 40,
              decoration: BoxDecoration(
                  color: Color(0xffFEC34D),
                  borderRadius: BorderRadius.circular(20)),
              child: const Center(
                  child: FaIcon(
                FontAwesomeIcons.fireAlt,
                color: Colors.white,
                size: 16,
              )),
            ),
            const SizedBox(
              width: 10,
            ),
            const Text("0", style: TextStyle(fontWeight: FontWeight.bold))
          ],
        )
      ]),
    );
  }
}

class HistorialRow extends StatelessWidget {
  final ResponseData data;

  const HistorialRow({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SizedBox(
      height: size.height * .29,
      child: ListView.builder(
        itemCount: data.results.length,
        itemBuilder: (BuildContext context, int index) {
          final sensorValue = data.results[index];
          var date = DateTime.fromMillisecondsSinceEpoch(
              data.results[index].timestamp);
          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                          color: const Color(0xffFF541E),
                          borderRadius: BorderRadius.circular(20)),
                      child: const Center(
                          child: FaIcon(
                        FontAwesomeIcons.temperatureHigh,
                        color: Colors.white,
                        size: 16,
                      )),
                    ),
                    Text(sensorValue.value.toString()),
                    Text(date.toString()),
                    const Icon(Icons.chevron_right)
                  ],
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}

class _SensorInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      Row(
        children: [
          Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
                color: const Color(0xffFF541E),
                borderRadius: BorderRadius.circular(20)),
            child: const Center(
                child: FaIcon(
              FontAwesomeIcons.temperatureHigh,
              color: Colors.white,
              size: 16,
            )),
          ),
          const SizedBox(
            width: 10,
          ),
          const Text("28°", style: TextStyle(fontWeight: FontWeight.bold))
        ],
      ),
      Row(
        children: [
          Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
                color: Color(0xff5ebbff),
                borderRadius: BorderRadius.circular(20)),
            child: const Center(
                child: FaIcon(
              FontAwesomeIcons.temperatureLow,
              color: Colors.white,
              size: 16,
            )),
          ),
          const SizedBox(
            width: 10,
          ),
          const Text("9°", style: TextStyle(fontWeight: FontWeight.bold))
        ],
      ),
    ]);
  }
}
