// To parse this JSON data, do
//
//     final sensorVariables = sensorVariablesFromMap(jsonString);

import 'dart:convert';

class SensorVariables {
  SensorVariables({
    required this.count,
    this.next,
    this.previous,
    required this.results,
  });

  int count;
  dynamic next;
  dynamic previous;
  List<Result> results;

  factory SensorVariables.fromJson(String str) =>
      SensorVariables.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory SensorVariables.fromMap(Map<String, dynamic> json) => SensorVariables(
        count: json["count"],
        next: json["next"],
        previous: json["previous"],
        results:
            List<Result>.from(json["results"].map((x) => Result.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "count": count,
        "next": next,
        "previous": previous,
        "results": List<dynamic>.from(results.map((x) => x.toMap())),
      };
}

class Result {
  Result({
    required this.id,
    required this.name,
    this.icon,
    this.unit,
    required this.label,
    required this.datasource,
    required this.url,
    required this.description,
    required this.properties,
    required this.tags,
    required this.createdAt,
    required this.lastValue,
    required this.lastActivity,
    required this.type,
    required this.derivedExpr,
    required this.valuesUrl,
  });

  String id;
  String name;
  dynamic icon;
  dynamic unit;
  String label;
  Datasource datasource;
  String url;
  String description;
  Properties properties;
  List<dynamic> tags;
  DateTime createdAt;
  LastValue lastValue;
  int lastActivity;
  int type;
  String derivedExpr;
  String valuesUrl;

  factory Result.fromJson(String str) => Result.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Result.fromMap(Map<String, dynamic> json) => Result(
        id: json["id"],
        name: json["name"],
        icon: json["icon"],
        unit: json["unit"],
        label: json["label"],
        datasource: Datasource.fromMap(json["datasource"]),
        url: json["url"],
        description: json["description"],
        properties: Properties.fromMap(json["properties"]),
        tags: List<dynamic>.from(json["tags"].map((x) => x)),
        createdAt: DateTime.parse(json["created_at"]),
        lastValue: LastValue.fromMap(json["last_value"]),
        lastActivity: json["last_activity"],
        type: json["type"],
        derivedExpr: json["derived_expr"],
        valuesUrl: json["values_url"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "icon": icon,
        "unit": unit,
        "label": label,
        "datasource": datasource.toMap(),
        "url": url,
        "description": description,
        "properties": properties.toMap(),
        "tags": List<dynamic>.from(tags.map((x) => x)),
        "created_at": createdAt.toIso8601String(),
        "last_value": lastValue.toMap(),
        "last_activity": lastActivity,
        "type": type,
        "derived_expr": derivedExpr,
        "values_url": valuesUrl,
      };
}

class Datasource {
  Datasource({
    required this.id,
    required this.name,
    required this.url,
    required this.label,
  });

  String id;
  String name;
  String url;
  String label;

  factory Datasource.fromJson(String str) =>
      Datasource.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Datasource.fromMap(Map<String, dynamic> json) => Datasource(
        id: json["id"],
        name: json["name"],
        url: json["url"],
        label: json["label"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "url": url,
        "label": label,
      };
}

class LastValue {
  LastValue({
    required this.value,
    required this.timestamp,
    required this.context,
    required this.createdAt,
  });

  double value;
  int timestamp;
  Properties context;
  int createdAt;

  factory LastValue.fromJson(String str) => LastValue.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory LastValue.fromMap(Map<String, dynamic> json) => LastValue(
        value: json["value"],
        timestamp: json["timestamp"],
        context: Properties.fromMap(json["context"]),
        createdAt: json["created_at"],
      );

  Map<String, dynamic> toMap() => {
        "value": value,
        "timestamp": timestamp,
        "context": context.toMap(),
        "created_at": createdAt,
      };
}

class Properties {
  Properties();

  factory Properties.fromJson(String str) =>
      Properties.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Properties.fromMap(Map<String, dynamic> json) => Properties();

  Map<String, dynamic> toMap() => {};
}
