// To parse this JSON data, do
//
//     final responseData = responseDataFromMap(jsonString);

import 'dart:convert';

class ResponseData {
  ResponseData({
    required this.count,
    this.next,
    this.previous,
    required this.results,
  });

  bool count;
  dynamic next;
  dynamic previous;
  List<Result> results;

  factory ResponseData.fromJson(String str) =>
      ResponseData.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory ResponseData.fromMap(Map<String, dynamic> json) => ResponseData(
        count: json["count"],
        next: json["next"],
        previous: json["previous"],
        results:
            List<Result>.from(json["results"].map((x) => Result.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "count": count,
        "next": next,
        "previous": previous,
        "results": List<dynamic>.from(results.map((x) => x.toMap())),
      };
}

class Result {
  Result({
    required this.timestamp,
    required this.value,
    required this.context,
    required this.createdAt,
  });

  int timestamp;
  double value;
  Context context;
  int createdAt;

  factory Result.fromJson(String str) => Result.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Result.fromMap(Map<String, dynamic> json) => Result(
        timestamp: json["timestamp"],
        value: json["value"],
        context: Context.fromMap(json["context"]),
        createdAt: json["created_at"],
      );

  Map<String, dynamic> toMap() => {
        "timestamp": timestamp,
        "value": value,
        "context": context.toMap(),
        "created_at": createdAt,
      };
}

class Context {
  Context();

  factory Context.fromJson(String str) => Context.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Context.fromMap(Map<String, dynamic> json) => Context();

  Map<String, dynamic> toMap() => {};
}
