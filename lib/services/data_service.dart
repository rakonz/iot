import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:iot_final/models/response_data.dart';
import 'package:iot_final/models/sensor_variables.dart';

class DataService {
  DataService() {}

  static Future getData() async {
    final queryParameter = {'page_size': '10000'};
    var url = Uri.https("industrial.api.ubidots.com",
        "/api/v1.6/devices/iot-grupo3/flama/values", queryParameter);
    var response = await http.get(url,
        headers: {"X-Auth-Token": "BBFF-6o6Pv05kGozO0Q7PIISc7jl1cjib9P"});
    print(response.body);
    final responseData = ResponseData.fromJson(response.body); //
    return responseData;
  }

  static Future getVariableData() async {
    final SensorVariables respData;
    var url = Uri.https(
      "industrial.api.ubidots.com",
      "/api/v1.6/datasources/6350475d746bd464ae72a571/variables",
    );
    var response = await http.get(url,
        headers: {"X-Auth-Token": "BBFF-6o6Pv05kGozO0Q7PIISc7jl1cjib9P"});
    // print(response.body);

    final responseData = SensorVariables.fromJson(response.body); //

    return responseData;
  }
}
