import 'package:flutter/material.dart';

class AppBarCustom extends StatelessWidget {
  final String name;
  final String profileImage;
  final int? notifications_count;

  const AppBarCustom(
      {Key? key,
      required this.name,
      required this.profileImage,
      this.notifications_count = 0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30),
      height: size.height * 0.17,
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
          blurRadius: 6,
          color: Color.fromARGB(255, 206, 205, 205),
        )
      ]),
      child: SafeArea(
        child: Padding(
          padding: EdgeInsets.only(
            left: 20,
            right: 20,
            top: 10,
          ),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: Container(
                    height: 30,
                    width: 30,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(50)),
                    child: Image(image: NetworkImage(this.profileImage)),
                  ),
                ),
                SizedBox(
                  width: size.width * 0.05,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Buenos días",
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ),
                    Text(
                      this.name,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ],
            ),
            Stack(
              children: [
                IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.notifications_outlined,
                      size: 24,
                    )),
                Positioned(
                  top: size.height * 0.01,
                  right: size.width * 0.022,
                  child: Container(
                    width: 14,
                    height: 14,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: Color(0xff5CADA8)),
                    child: Center(
                        child: Text(
                      "2",
                      style: TextStyle(
                          fontSize: 10,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    )),
                  ),
                ),
              ],
            )
          ]),
        ),
      ),
    );
  }
}
